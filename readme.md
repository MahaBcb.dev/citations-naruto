# Citations API

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)  [![forthebadge](http://forthebadge.com/images/badges/powered-by-electricity.svg)](http://forthebadge.com)

Cette appli servira à créer un bot qui postera aléatoirement des citations de manga sur ses réseaux

## Pour commencer


### Pré-requis

Ce qui est requis pour commencer avec votre projet...

- composer
- symfony CLI
- php 8.0.23
- mysql 8.0
- docker 
- npm
- webpack
- api platform

### Installation

- ``composer install`` (pour installer les dépendances et librairies requises)
- ``docker-compose up -d`` (pour lancer la base de données dockerisé et le mail catcher dockerisé)

## Démarrage

- ``npm install``
- ``npm run build``
- ``symfony console doctrine:migrations:migrate``
- ``symfony console doctrine:fixtures:load``
- ``symfony serve -d`` (pour lancer le serveur symfony en local)

## Test unitaire

- ``php bin/phpunit``

## Amelioration
- Mettre en place un moteur de recherche typesense
- Mettre en place une reponse aleatoire du bot lors d'une mention twitter
- Automatiser la tache du bot avec Cron
- Ajouter une validation
- style login page admin
- token jwt pour authentifier

## License

Ce projet est sous licence ``exemple: WTFTPL`` - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations
