<?php

namespace App\DataFixtures;

use App\Entity\Citation;
use App\Entity\Personnage;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;
use Faker;

class AppFixtures extends Fixture
{
    private $hash;

    public function __construct(UserPasswordHasherInterface $hash)
    {
        $this->hash = $hash;
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        $user = new User();
        $password = $this->hash->hashPassword($user, 'password');
        $user
            ->setEmail("john.doe@mail.fr")
            ->setPassword($password)
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        for ($j = 0; $j < 5; $j++) {
            $personnage = new Personnage();
            $personnage
                    ->setFullName($faker->firstName() . " " .$faker->lastName() )
                    ->setVillage($faker->city())
                    ->setImage($faker->word());
                    $manager->persist($personnage);
        }

        $manager->flush();
    }
}
