<?php

namespace App\Command;

use App\Service\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:create-admin',
    description: 'Creates a new admin.',
    hidden: false,
    aliases: ['app:add-admin']
)]
class CreateAdminCommand extends Command
{
    public function __construct(public UserManager $userManager)
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'L\'adresse email de l\'admin')
            ->addArgument('password', InputArgument::REQUIRED, 'Le mot de passe de l\'admin');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        $email = $input->getArgument('email');
        $password = $input->getArgument('password');

        $user = $this->userManager->createUser($email, $password);

        $output->writeln([
            'User created successfully!',
            '',
            'Username: ' . $user->getEmail(),
        ]);

        return Command::SUCCESS;
    }
}
