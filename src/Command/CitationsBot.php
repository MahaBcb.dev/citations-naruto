<?php

namespace App\Command;

use App\Service\GetCitationsService;
use App\Service\TwitterApiService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CitationsBot extends Command
{
    private $getCitationsService;
    private $twitterApi;
    protected static $defaultName = 'bot:post';

    public function __construct(
        GetCitationsService $getCitationsService,
        TwitterApiService $twitterApi
    ) {
        parent::__construct();
        $this->getCitationsService = $getCitationsService;
        $this->twitterApi = $twitterApi;
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $content = $this->getCitationsService->fromCitations();
        $this->twitterApi->post($content . " #Naruto #NarutoShippuden");
        return Command::SUCCESS;
    }
}
