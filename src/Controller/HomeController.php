<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\Citation;
use App\Entity\Personnage;
use App\Form\CitationFormType;
use Symfony\Component\Mime\Email;
use App\Service\GetCitationsService;
use App\Repository\CitationRepository;
use App\Repository\PersonnageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    #[Route('/', name: 'app_home')]
    public function home(
        MailerInterface $mailer,
        CitationRepository $citationRepository,
        PersonnageRepository $personnageRepository,
        Request $request
    ): Response {
        $citation = new Citation();
        $form = $this->createForm(CitationFormType::class, $citation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $citation->setUser($this->getUser());
            $citation->setCreatedAt(new DateTime("now"));

            $email = (new Email())
            ->from('hello@example.com')
            ->to('admin@mail.fr')
            ->subject('Une nouvelle citation a été créée')
            ->text( "Nouvelle citation: " . $citation->getPersonnages()->getFullName() . ": " . $citation->getDescription());
            $mailer->send($email);
            $this->addFlash("primary", "Votre citation a bien été prise en compte. Merci pour votre participation!");

            return $this->redirectToRoute('app_home');
        }

        // $citation = $this->getCitations->fromCitations();
        $personnages = $personnageRepository->findPerso();
        $citations = $citationRepository->findByDate();

        return $this->render('home/index.html.twig', [
            'citations' => $citations,
            'personnages' => $personnages,
            'form' => $form->createView()
        ]);
    }
}
