<?php

namespace App\Controller;

use App\Entity\Personnage;
use App\Repository\CitationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PersonnageRepository;

class PersonnagesController extends AbstractController
{
    #[Route('/personnages', name: 'app_personnages')]
    public function index(PersonnageRepository $personnagesRepository): Response
    {
        $personnages = $personnagesRepository->findAll();
        return $this->render('personnages/index.html.twig', [
            'personnages' => $personnages,
        ]);
    }

    #[Route('/personnages/{id<[0-9]+>}', name: 'app_personnages_show', methods:['GET'])]
    public function show(Personnage $personnage, CitationRepository $citationRepository): Response
    {
        $id = $personnage->getId();
        $citations = $citationRepository->showCitation($id);

        return $this->render('personnages/show.html.twig', [
            'personnage' => $personnage,
            'citations' => $citations
        ]);
    }
}
