<?php

namespace App\Controller\Admin;

use App\Entity\Citation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CitationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Citation::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('description'),
            AssociationField::new('personnages'),
            AssociationField::new('user', 'Utilisateur')->hideOnForm()
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
                ->setDefaultSort(['createdAt' => 'DESC']);
    }
}
