<?php

namespace App\Controller;

use App\Repository\CitationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use ACSEO\TypesenseBundle\Finder\TypesenseQuery;
use ACSEO\TypesenseBundle\Finder\CollectionFinder;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Citation;
use App\Form\CitationFormType;

#[ApiResource()]

class CitationsController extends AbstractController
{
    private $citationsFinder;

    #public function __construct(CollectionFinder $citationsFinder)
    #{
    #    $this->citationsFinder = $citationsFinder;
    #}

    #[Route('/citations', name: 'app_citations')]
    public function index(
        CitationRepository $citationRepository,
        Request $request,
        PaginatorInterface $paginator
    ): Response {


        #$query = new TypesenseQuery('consequatur', 'description');
        #$results = $this->citationsFinder->rawQuery($query)->getResults();


        $data = $citationRepository->findAll();
        $citations = $paginator->paginate($data, $request->query->getInt('page', 1), 10);

        return $this->render('citations/index.html.twig', [
            'citations' => $citations,
            #'results' => $results,
        ]);
    }
}
