<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PersonnageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PersonnageRepository::class)]
#[ApiResource(
    collectionOperations:[
        "get",
        "post"
    ],
    itemOperations:[
        "get",
        "put"
    ],
    normalizationContext:["groups" => ["personnage:read"]],
    denormalizationContext:["groups" => ["personnage:write"]]
)]

class Personnage
{
    #[Groups(["personnage:read"])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(["personnage:read", "personnage:write", "citation:read"])]
    #[ORM\Column(type: 'string', length: 255)]
    private $fullName;

    #[Groups(["personnage:read", "personnage:write"])]
    #[ORM\Column(type: 'string', length: 255)]
    private $village;

    #[Groups(["personnage:read", "personnage:write"])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $image;

    #[Groups(["personnage:read", "personnage:write"])]
    #[ORM\OneToMany(mappedBy: 'personnages', targetEntity: Citation::class, orphanRemoval: true)]
    private $citations;

    public function __construct()
    {
        $this->citations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function getVillage(): ?string
    {
        return $this->village;
    }

    public function setVillage(string $village): self
    {
        $this->village = $village;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, Citation>
     */
    public function getCitations(): Collection
    {
        return $this->citations;
    }

    public function addCitation(Citation $citation): self
    {
        if (!$this->citations->contains($citation)) {
            $this->citations[] = $citation;
            $citation->setPersonnages($this);
        }

        return $this;
    }

    public function removeCitation(Citation $citation): self
    {
        if ($this->citations->removeElement($citation)) {
            // set the owning side to null (unless already changed)
            if ($citation->getPersonnages() === $this) {
                $citation->setPersonnages(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->fullName ;
    }
}
