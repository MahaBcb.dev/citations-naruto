<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\CitationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CitationRepository::class)]
#[ApiResource(
    collectionOperations:[
        "get",
        "post"
    ],
    itemOperations:[
        "get",
        "put"
    ],
    normalizationContext:["groups" => ["citation:read"]],
    denormalizationContext:["groups" => ["citation:write"]]
)]
class Citation
{
    #[Groups(["citation:read"])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(["citation:read", "citation:write"])]
    #[ORM\Column(type: 'text')]
    private $description;

    #[Groups(["citation:read"])]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'citations')]
    #[ORM\JoinColumn(nullable: true)]
    private $user;

    #[Groups(["citation:read", "citation:write", "pero"])]
    #[ORM\ManyToOne(targetEntity: Personnage::class, inversedBy: 'citations')]
    #[ORM\JoinColumn(nullable: false)]
    private $personnages;

    #[Groups(["citation:read"])]
    #[ORM\Column(type: 'datetime', nullable: true)]
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPersonnages(): ?Personnage
    {
        return $this->personnages;
    }

    public function setPersonnages(?Personnage $personnages): self
    {
        $this->personnages = $personnages;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function __toString(): string
    {
        return $this->description ;
    }
}
