<?php

namespace App\Service;

use App\Repository\CitationRepository;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetCitationsService
{
    private $client;
    private $citationRepository;

    public function __construct(
        HttpClientInterface $client,
        CitationRepository $citationRepository
    ) {
        $this->client = $client;
        $this->citationRepository = $citationRepository;
    }

    public function fromCitations(): string
    {

        $nombreId = $this->citationRepository->nbreCitations();
        $randomId = rand(1, $nombreId);
        $lien = 'https://citations-api-manga.herokuapp.com/api/citations/' . $randomId;
        $response = $this->client->request(
            'GET',
            $lien
        );
        $description = $response->toArray()["description"];
        $personnage = $response->toArray()["personnages"]["nom"];
        return $description;
    }
}
