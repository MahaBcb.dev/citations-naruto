<?php

namespace App\Tests;

use App\Entity\Citation;
use PHPUnit\Framework\TestCase;

class CitationUnitTest extends TestCase
{
    public function testCitationIsTrue(): void
    {
        $citation = new Citation();
        $citation->setDescription('description');

        $this->assertTrue($citation->getDescription() === 'description');
    }

    public function testCitationIsFalse(): void
    {
        $citation = new Citation();
        $citation->setDescription('description');

        $this->assertFalse($citation->getDescription() === 'false');
    }

    public function testCitationIsEmpty(): void
    {
        $citation = new Citation();

        $this->assertEmpty($citation->getDescription());
    }
}
