<?php

namespace App\Tests;

use App\Entity\Citation;
use App\Entity\Personnage;
use PHPUnit\Framework\TestCase;

class PersonnageUnitTest extends TestCase
{
    public function testPersonnageIsTrue(): void
    {
        $citation = new Citation();
        $personnage = new Personnage();
        $personnage->setNom('nom')
                ->setPrenom('prenom')
                ->setImage('file')
                ->addCitation($citation)
                ->setVillage('village');

        $this->assertTrue($personnage->getNom() === 'nom');
        $this->assertTrue($personnage->getPrenom() === 'prenom');
        $this->assertTrue($personnage->getVillage() === 'village');
        $this->assertTrue($personnage->getImage() === 'file');
    }

    public function testPersonnageIsFalse(): void
    {
        $citation = new Citation();
        $personnage = new Personnage();
        $personnage->setNom('nom')
                ->setPrenom('prenom')
                ->setImage('file')
                ->addCitation($citation);

        $this->assertFalse($personnage->getNom() === 'false');
        $this->assertFalse($personnage->getPrenom() === 'false');
        $this->assertFalse($personnage->getCitations() === false);
        $this->assertFalse($personnage->getImage() === 'false');
    }

    public function testPersonnageIsEmpty(): void
    {
        $personnage = new Personnage();

        $this->assertEmpty($personnage->getNom());
        $this->assertEmpty($personnage->getPrenom());
        $this->assertEmpty($personnage->getCitations());
        $this->assertEmpty($personnage->getImage());
    }
}
